<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


class DistanceCalculator {

	/**
	 * @param array  $from
	 * @param array  $to
	 * @param string $unit - m, km
	 *
	 * @return mixed
	 */
	public function calculate( $from, $to, $unit = 'm' ) {
		$lat1= $from[0];
		$lon1 = $from[1];
		$lat2= $to[0];
		$lon2 = $to[1];

		$R = 6371e3;
		$o1 = $lat1 * pi() / 180;
		$o2 = $lat2 * pi() / 180;
		$do = ($lat2 - $lat1) * pi() / 180;
		$db = ($lon2 - $lon1) * pi() / 180;
		$a = sin($do/2) * sin($do/2) + cos($o1) * cos($o2) * sin($db/2) * sin($db/2);
		$c = 2 * atan2(sqrt($a), sqrt(1-$a));
		$distance = $R * $c;
		if($unit === "km")
		{
			$distance = $distance / 1000;
		}
		return $distance;
	}

	/**
	 * @param array $from
	 * @param array $offices
	 *
	 * @return array
	 */
	public function findClosestOffice( $from, $offices ) {
		$closestOffice = null;
		$currentMinDistance = null;
		foreach ($offices as $office) {
			$currentDistance = $this->calculate($from,[$office['lat'],$office['lng'],"m"]);
			if($currentMinDistance == null || $currentMinDistance > $currentDistance)
			{
				$closestOffice = $office;
				$currentMinDistance = $currentDistance;
			}
		}
		
		return $closestOffice;
	}



}