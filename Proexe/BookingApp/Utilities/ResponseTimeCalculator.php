<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface {

	private $msToDay = 86400;
	private $sToMinutes = 60;

	//Write your methods here

	/**
	 * @param string  $bookingDateTime
	 * @param string  $responseDateTime
	 * @param array  $officeHours
	 *
	 * @return int
	 */
	public function calculate($bookingDateTime, $responseDateTime, $officeHours)
    {
		
		$daysDifference = $this->calcDaysDifference($bookingDateTime,$responseDateTime);
	
		$d1 = $this->calcTimeStamp($bookingDateTime, $officeHours);
		$d2 = $this->calcTimeStamp($responseDateTime, $officeHours);
		$addationalTime = 0;

		
		if($daysDifference >1)
		{
			for ($i=0; $i < $daysDifference-1; $i++) { 
				$newDate = date("Y-m-d",strtotime($daysDifference) + $this->msToDay) . "23:59";
				$addationalTime += $this->calcTimeStamp($newDate, $officeHours);
			}
			
		}
		$responseTime = ceil(($d2 + $d1 + $addationalTime) / $this->sToMinutes);
		

		return $responseTime;
		
		
	}

	/**
	 * @param array  $date
	 * @param array  $officeHours
	 *
	 * @return array
	 */
	private function parseDateForTimeStamp($date, $officeHours)
	{

		$dateTimeStamp = strtotime($date);
		$dateDay = date("w", $dateTimeStamp);
		if($officeHours[$dateDay]['isClosed']) return ['dateTimeStamp' => 0, 'toOpenTimeStamp' =>0, 'fromOpenTimeStamp' => 0 ];

		$dateDate = date("Y-m-d", $dateTimeStamp);
		$fromTimes = $officeHours[$dateDay]['from'];
		$fromOpenTimeStamp = strtotime($dateDate . " " .$fromTimes);

		$toTimes = $officeHours[$dateDay]['to'];
		$toOpenTimeStamp = strtotime($dateDate . " " .$toTimes );
		return ['dateTimeStamp' => $dateTimeStamp, 'toOpenTimeStamp' => $toOpenTimeStamp, 'fromOpenTimeStamp' => $fromOpenTimeStamp ];
	}

	/**
	 * @param array  $date
	 * @param array  $officeHours
	 *
	 * @return int
	 */
	private function calcTimeStamp($date, $officeHours)
	{
		
		$parsedDates = $this->parseDateForTimeStamp($date, $officeHours);
		$dateTimeStamp = $parsedDates['dateTimeStamp'];
		$toOpenTimeStamp = $parsedDates['toOpenTimeStamp'];
		$fromOpenTimeStamp = $parsedDates['fromOpenTimeStamp'];

		if($dateTimeStamp >= $toOpenTimeStamp && $dateTimeStamp <= $fromOpenTimeStamp)
		{
			return $dateTimeStamp - $fromOpenTimeStamp;
		}
		if( $dateTimeStamp > $fromOpenTimeStamp)
		{
			return $toOpenTimeStamp - $fromOpenTimeStamp; 
		}
		return 0;
	}

	/**
	 * @param string  $bookingDateTime
	 * @param string  $responseDateTime
	 *
	 * @return int
	 */
	private function calcDaysDifference($bookingDateTime,$responseDateTime)
	{
	
		$bookingDateTimeTimeStamp = strtotime($bookingDateTime);
		$bookingDate = strtotime(date("Y-m-d", $bookingDateTimeTimeStamp));
		
		$responseDateTimeTimeStamp = strtotime($responseDateTime);
		$responseDate = strtotime(date("Y-m-d", $responseDateTimeTimeStamp));
	
		return ceil(($responseDate - $bookingDate) / $this->msToDay);
	}
}